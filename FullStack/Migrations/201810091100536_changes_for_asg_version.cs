namespace AsgPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changes_for_asg_version : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Gigs", "ArtistId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Gigs", "GenreId", "dbo.Genres");
            DropForeignKey("dbo.Attendances", "GigId", "dbo.Gigs");
            DropIndex("dbo.Attendances", new[] { "GigId" });
            DropIndex("dbo.Gigs", new[] { "ArtistId" });
            DropIndex("dbo.Gigs", new[] { "GenreId" });
            DropPrimaryKey("dbo.Attendances");
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrganizerId = c.String(nullable: false, maxLength: 128),
                        DateTime = c.DateTime(nullable: false),
                        Venue = c.String(nullable: false, maxLength: 255),
                        GenreId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Genres", t => t.GenreId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.OrganizerId, cascadeDelete: true)
                .Index(t => t.OrganizerId)
                .Index(t => t.GenreId);
            
            AddColumn("dbo.Attendances", "EventId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Attendances", new[] { "EventId", "AttendeeId" });
            CreateIndex("dbo.Attendances", "EventId");
            AddForeignKey("dbo.Attendances", "EventId", "dbo.Events", "Id");
            DropColumn("dbo.Attendances", "GigId");
            DropTable("dbo.Gigs");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Gigs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArtistId = c.String(nullable: false, maxLength: 128),
                        DateTime = c.DateTime(nullable: false),
                        Venue = c.String(nullable: false, maxLength: 255),
                        GenreId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Attendances", "GigId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Attendances", "EventId", "dbo.Events");
            DropForeignKey("dbo.Events", "OrganizerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Events", "GenreId", "dbo.Genres");
            DropIndex("dbo.Events", new[] { "GenreId" });
            DropIndex("dbo.Events", new[] { "OrganizerId" });
            DropIndex("dbo.Attendances", new[] { "EventId" });
            DropPrimaryKey("dbo.Attendances");
            DropColumn("dbo.Attendances", "EventId");
            DropTable("dbo.Events");
            AddPrimaryKey("dbo.Attendances", new[] { "GigId", "AttendeeId" });
            CreateIndex("dbo.Gigs", "GenreId");
            CreateIndex("dbo.Gigs", "ArtistId");
            CreateIndex("dbo.Attendances", "GigId");
            AddForeignKey("dbo.Attendances", "GigId", "dbo.Gigs", "Id");
            AddForeignKey("dbo.Gigs", "GenreId", "dbo.Genres", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Gigs", "ArtistId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
