namespace AsgPortal.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulateGenres : DbMigration
    {
        public override void Up()
        {
            Sql("Insert INTO Genres (Id, Name) Values (1, 'Deathmach')");
            Sql("Insert INTO Genres (Id, Name) Values (2, 'Team Deathmach')");
            Sql("Insert INTO Genres (Id, Name) Values (3, 'Custom Scenario')");
            Sql("Insert INTO Genres (Id, Name) Values (4, 'We will see')");
        }

        public override void Down()
        {
            Sql("DELETE FROM Genres Where Id IN (1, 2, 3. 4)");
        }
    }
}
