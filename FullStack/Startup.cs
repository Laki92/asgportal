﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AsgPortal.Startup))]
namespace AsgPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
