﻿using AsgPortal.Models;
using AsgPortal.ViewModel;
using FullStack.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace FullStack.Controllers
{
    public class FolloweesController : Controller
    {
        private ApplicationDbContext _context;

        public FolloweesController()
        {
            _context = new ApplicationDbContext();
        }

        [Authorize]
        public ActionResult Following()
        {
            var userId = User.Identity.GetUserId();
            var events = _context.Attendances
                .Where(a => a.AttendeeId == userId)
                .Select(a => a.Event)
                .Include(g => g.Organizer)
                .Include(g => g.Genre)
                .ToList();

            var followees = _context.Followings.Where(a => a.FollowerId == userId).Select(a => a.FolloweeId);

            var EventHelperList = new List<EventHelper>();

            foreach (var singleEvent in events)
            {
                var following = false;

                if (followees.Contains(singleEvent.OrganizerId))
                    following = true;

                EventHelperList.Add(new EventHelper() { UpcomingEvent = singleEvent, IsGoing = true, IsFollowing = following });
            }

            var viewModel = new HomeViewModel()
            {
                UpcomingEvents = EventHelperList,
                ShowActions = User.Identity.IsAuthenticated,
                Heading = "Peoples I'm following",
            };

            return View("Followees", viewModel);
        }
    }
}