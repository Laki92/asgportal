﻿using AsgPortal.Models;
using AsgPortal.ViewModel;
using FullStack.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace AsgPortal.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController()
        {
            _context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();

            var events = _context.Attendances
                .Where(a => a.AttendeeId == userId)
                .Select(a => a.Event.Id).ToList();

            var followees = _context.Followings.Where(a => a.FollowerId == userId).Select(a => a.FolloweeId).ToList();

            var EventHelperList = new List<EventHelper>();

            var upcomingEvents = _context.Events
                .Include(g => g.Organizer)
                .Include(g => g.Genre)
                .Where(g => g.DateTime > DateTime.Now);

            foreach (var singleEvent in upcomingEvents)
            {
                var following = false;
                var going = false;

                if (followees.Contains(singleEvent.OrganizerId))
                    following = true;

                if (events.Contains(singleEvent.Id))
                    going = true;

                EventHelperList.Add(new EventHelper() { UpcomingEvent = singleEvent, IsGoing = going, IsFollowing = following });
            }

            var viewModel = new HomeViewModel
            {
                UpcomingEvents = EventHelperList,
                ShowActions = User.Identity.IsAuthenticated,
                Heading = "Upcoming Events"
            };

            return View("Events", viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}