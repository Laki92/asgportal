﻿using AsgPortal.Dtos;
using AsgPortal.Models;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Http;

namespace AsgPortal.Controllers
{
    [System.Web.Http.Authorize]
    public class FollowingsController : ApiController
    {
        private ApplicationDbContext _context;

        public FollowingsController()
        {
            _context = new ApplicationDbContext();
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult Follow(FollowingDto dto)
        {
            var userId = User.Identity.GetUserId();

            if (_context.Followings.Any(f => f.FolloweeId == userId && f.FolloweeId == dto.FolloweeId.ToString()))
            {
                var followeeToRemove = _context.Followings.Where(a => a.FollowerId == userId && a.FolloweeId == dto.FolloweeId).FirstOrDefault();
                _context.Followings.Remove(followeeToRemove);
                _context.SaveChanges();
                return BadRequest("removed from following");
            }


            var following = new Following
            {
                FollowerId = userId,
                FolloweeId = dto.FolloweeId
            };

            _context.Followings.Add(following);
            _context.SaveChanges();

            return Ok();
        }
    }
}
