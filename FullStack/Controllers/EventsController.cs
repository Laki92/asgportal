﻿using AsgPortal.Models;
using AsgPortal.ViewModel;
using FullStack.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace AsgPortal.Controllers
{
    public class EventsController : Controller
    {
        private ApplicationDbContext _context;

        public EventsController()
        {
            _context = new ApplicationDbContext();
        }

        [Authorize]
        public ActionResult Attending()
        {
            var userId = User.Identity.GetUserId();
            var events = _context.Attendances
                .Where(a => a.AttendeeId == userId)
                .Select(a => a.Event)
                .Include(g => g.Organizer)
                .Include(g => g.Genre)
                .ToList();

            var followees = _context.Followings.Where(a => a.FollowerId == userId).Select(a => a.FolloweeId);

            var EventHelperList = new List<EventHelper>();

            foreach (var singleEvent in events)
            {
                var following = false;

                if (followees.Contains(singleEvent.OrganizerId))
                    following = true;

                EventHelperList.Add(new EventHelper() { UpcomingEvent = singleEvent, IsGoing = true, IsFollowing = following });
            }

            var viewModel = new HomeViewModel()
            {
                UpcomingEvents = EventHelperList,
                ShowActions = User.Identity.IsAuthenticated,
                Heading = "Events I'm going",
            };


            return View("Events", viewModel);
        }

        [Authorize]
        public ActionResult MyEvents()
        {
            var userId = User.Identity.GetUserId();

            var events = _context.Events.Where(x => x.OrganizerId == userId)
                .Include(o => o.Organizer)
                .Include(g => g.Genre);

            var attendances = _context.Attendances
                .Where(a => a.AttendeeId == userId)
                .Select(a => a.Event.Id).ToList();

            var followees = _context.Followings.Where(a => a.FollowerId == userId).Select(a => a.FolloweeId).ToList();


            var EventHelperList = new List<EventHelper>();

            foreach (var singleEvent in events)
            {
                var following = false;
                var going = false;

                if (followees.Contains(singleEvent.OrganizerId))
                    following = true;

                if (attendances.Contains(singleEvent.Id))
                    going = true;

                EventHelperList.Add(new EventHelper() { UpcomingEvent = singleEvent, IsGoing = going, IsFollowing = following });
            }

            var viewModel = new HomeViewModel()
            {
                UpcomingEvents = EventHelperList,
                ShowActions = User.Identity.IsAuthenticated,
                Heading = "My events",
            };

            return View("Events", viewModel);
        }


        [Authorize]
        public ActionResult Create()
        {
            var viewModel = new EventFormViewModel
            {
                Genres = _context.Genres.ToList()
            };
            return View(viewModel);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EventFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.Genres = _context.Genres.ToList();
                return View("Create", viewModel);
            }

            CultureInfo cultureinfo = new CultureInfo("pl-PL");

            var Event = new Event
            {
                OrganizerId = User.Identity.GetUserId(),
                DateTime = DateTime.Parse(string.Format("{0} {1}", viewModel.Date, viewModel.Time), cultureinfo),
                GenreId = viewModel.GenreId,
                Venue = viewModel.Venue
            };

            _context.Events.Add(Event);
            _context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }
    }
}