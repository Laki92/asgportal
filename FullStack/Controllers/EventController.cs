﻿using AsgPortal.Models;
using AsgPortal.ViewModel;
using Google.Maps;
using Google.Maps.StaticMaps;
using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace FullStack.Controllers
{
    public class EventController : Controller
    {
        private ApplicationDbContext _context;

        public EventController()
        {
            _context = new ApplicationDbContext();
        }

        [Authorize]
        public ActionResult SingleEvent(int id)
        {
            var GMapsKey = "AIzaSyDqGnk1E2TpYRI_HxVCmNEl3ITh2PfB1cA";

            var singleEvent = _context.Events
                .Where(a => a.Id == id)
                .Include(g => g.Organizer)
                .Include(g => g.Genre)
                .FirstOrDefault();

            var test1 = singleEvent.DateTime.Date.ToString("dd'.'MM'.'yyyy");
            var test2 = singleEvent.DateTime.TimeOfDay.ToString(@"hh\:mm");

            var showActions = false;

            if (User.Identity.IsAuthenticated && (singleEvent.OrganizerId == User.Identity.GetUserId()))
                showActions = true;

            var viewModel = new EventFormViewModel()
            {
                Venue = singleEvent.Venue,
                Date = singleEvent.DateTime.Date.ToString("dd'.'MM'.'yyyy"),
                Time = singleEvent.DateTime.TimeOfDay.ToString(@"hh\:mm"),
                Genres = _context.Genres.ToList(),
                GenreId = singleEvent.Genre.Id,
                EventId = singleEvent.Id,
                ShowActions = showActions
            };

            GoogleSigned.AssignAllServices(new GoogleSigned(GMapsKey));
            var map = new StaticMapRequest();
            map.Center = new Location(singleEvent.Venue);
            map.Size = new System.Drawing.Size(500, 500);
            map.Zoom = 14;
            map.Markers.Add(new Location(singleEvent.Venue));
            ViewBag.StaticMap = map.ToUriSigned(new GoogleSigned(GMapsKey));

            return View("Event", viewModel);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(EventFormViewModel viewModel)
        {

            if (!ModelState.IsValid)
            {
                viewModel.Genres = _context.Genres.ToList();
                return View("Event", viewModel);
            }

            var singleEvent = _context.Events
                .Where(a => a.Id == viewModel.EventId)
                .Include(g => g.Organizer)
                .Include(g => g.Genre)
                .FirstOrDefault();

            CultureInfo cultureinfo = new CultureInfo("pl-PL");

            singleEvent.OrganizerId = User.Identity.GetUserId();
            singleEvent.DateTime = DateTime.Parse(string.Format("{0} {1}", viewModel.Date, viewModel.Time), cultureinfo);
            singleEvent.GenreId = viewModel.GenreId;
            singleEvent.Venue = viewModel.Venue;

            _context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }
    }
}