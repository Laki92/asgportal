﻿using AsgPortal.Dtos;
using AsgPortal.Models;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Http;

namespace AsgPortal.Controllers
{
    [Authorize]
    public class AttendancesController : ApiController
    {
        private ApplicationDbContext _context;

        public AttendancesController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult Attend(AttendanceDto dto)
        {
            var userId = User.Identity.GetUserId();

            if (_context.Attendances.Any(a => a.AttendeeId == userId && a.EventId == dto.EventId))
            {
                var attendanceToRemove = _context.Attendances.Where(a => a.AttendeeId == userId && a.EventId == dto.EventId).FirstOrDefault();
                _context.Attendances.Remove(attendanceToRemove);
                _context.SaveChanges();
                return BadRequest("attendance removed");

            }

            var attendance = new Attendance
            {
                EventId = dto.EventId,
                AttendeeId = userId
            };

            _context.Attendances.Add(attendance);
            _context.SaveChanges();

            return Ok();
        }
    }
}
