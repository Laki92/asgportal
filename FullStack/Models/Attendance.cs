﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsgPortal.Models
{
    public class Attendance
    {
        public Event Event { get; set; }
        public ApplicationUser Attendee { get; set; }

        [Key]
        [Column(Order = 1)]
        public int EventId { get; set; }

        [Column(Order = 2)]
        [Key]
        public string AttendeeId { get; set; }
    }
}