﻿using AsgPortal.Models;

namespace FullStack.Models
{
    public class EventHelper
    {
        public Event UpcomingEvent { get; set; }
        public bool IsGoing { get; set; }
        public bool IsFollowing { get; set; }
    }
}