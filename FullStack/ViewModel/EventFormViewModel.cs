﻿using AsgPortal.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AsgPortal.ViewModel
{
    public class EventFormViewModel
    {
        [Required]
        public string Venue { get; set; }

        [Required]
        [FutureDate]
        public string Date { get; set; }

        [Required]
        [ValidTime]
        public string Time { get; set; }

        [Display(Name = "Genre")]
        [Required]
        public byte GenreId { get; set; }

        [ScaffoldColumn(false)]
        public int EventId { get; set; }

        public IEnumerable<Genre> Genres { get; set; }

        [ScaffoldColumn(false)]
        public bool ShowActions { get; set; }
    }
}