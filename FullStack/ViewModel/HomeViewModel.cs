﻿using FullStack.Models;
using System.Collections.Generic;

namespace AsgPortal.ViewModel
{
    public class HomeViewModel
    {
        public IEnumerable<EventHelper> UpcomingEvents { get; set; }
        public bool ShowActions { get; set; }
        public string Heading { get; set; }

    }
}